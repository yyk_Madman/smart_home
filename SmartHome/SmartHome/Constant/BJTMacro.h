//
//  BJTMacro.h
//  SmartHome
//
//  Created by Yale on 2018/11/27.
//  Copyright © 2018年 Kevin. All rights reserved.
//

#ifndef BJTMacro_h
#define BJTMacro_h


#define KiPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

//获取屏幕 宽度、高度
#define kScreen_frame ([UIScreen mainScreen].applicationFrame)
#define kSCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define kSCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define kSCREEN_BOUNDS ([UIScreen mainScreen].bounds)

//十六进制 色值
#define UIColorFromHexadecimalRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB色值
#define UIRGBAColor(r, g, b, a) [UIColor colorWithRed:(CGFloat)r/255.f green:(CGFloat)g/255.f blue:(CGFloat)b/255.f alpha:(CGFloat)a]
#define UIRGBColor(red, green, blue) UIRGBAColor(red, green, blue, 1)

#define FontWithSize(size) [UIFont systemFontOfSize:size]
#define SHORTVERSION [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

//  创建 __weak类型的对象
#define createWeak(o) __weak typeof(o) weak##o = o;
#define createStrong(o) __strong typeof(o) weak##o = o;

//获取视图的高
#define GETHEIGHT(view) CGRectGetHeight(view.frame)
//获取视图的宽
#define GETWIDTH(view) CGRectGetWidth(view.frame)
//获取视图的X坐标
#define GETORIGIN_X(view) view.frame.origin.x
//获取视图的Y坐标
#define GETORIGIN_Y(view) view.frame.origin.y
#define kNetworkErrorMessage    @"网络连接失败，你检查网路设置"

#endif /* BJTMacro_h */
