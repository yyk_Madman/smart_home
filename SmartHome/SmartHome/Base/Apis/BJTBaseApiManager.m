//
//  BJTBaseApiManager.m
//  SmartHome
//
//  Created by Yale on 2018/11/26.
//  Copyright © 2018年 Kevin. All rights reserved.
//

#import "BJTBaseApiManager.h"
#import "BJTBaseServiceManager.h"
@implementation BJTBaseApiManager
@synthesize serviceConfig = _serviceConfig;
- (id)serviceConfig {
    if (!_serviceConfig) {
        _serviceConfig = [BJTBaseServiceManager new];
    }
    return _serviceConfig;
}
@end
