//
//  BJTBaseServiceManager.m
//  SmartHome
//
//  Created by Yale on 2018/11/26.
//  Copyright © 2018年 Kevin. All rights reserved.
//

#import "BJTBaseServiceManager.h"

@implementation BJTBaseServiceManager
- (NSNumber *)isOnline {
    return @([[TBAPIServiceSettingManager sharedInstance] isOnlineApi]);
}

- (NSString *)onlineApiDomain {
#ifdef DEBUG
    return TBDefaultUATDomain;
#else
    return TBReleaseOnlineDomain;
#endif
}

- (NSString *)onlineApiWebDomain {
#ifdef DEBUG
    return TBDefaultUATDomain;
#else
    return TBReleaseOnlineDomain;
#endif
}

- (NSString *)onlineServiceName {
    return @"";
}

- (NSString *)onlinePublicKey {
    return @"";
}

- (NSString *)onlinePrivateKey {
    return @"";
}

- (NSString *)offlineApiDomain {
    NSString *domainUAT = [[NSUserDefaults standardUserDefaults] objectForKey:@""];
    if (!domainUAT) {
        domainUAT = TBDefaultUATDomain;
    }
    return domainUAT;
}

- (NSString *)offlineApiWebDomain {
    return [self onlineApiWebDomain];
}

- (NSString *)offlineServiceName {
    return [self onlineServiceName];
}

- (NSString *)offlinePublicKey {
    return [self onlinePublicKey];
}

- (NSString *)offlinePrivateKey {
    return [self onlinePrivateKey];
}

- (NSDictionary *)commonRequestHeader {
    return @{
             
             };
}

- (NSString *)getLocalizedDescriptionWithStatusCode:(NSInteger)statusCode {
    switch (statusCode) {
        case 404:
        case NSURLErrorNotConnectedToInternet:
        case NSURLErrorTimedOut:
            NSLog(@"网络请求错误码：code=%ld", (long)statusCode);
            return @"网络连接似乎出了点问题，请检查后重试";
        default:
            break;
    }
    NSString *statusMessage = @"";
    if ([self isOnline].integerValue == 0) {
        if (statusMessage.length == 0) {
            statusMessage = [NSString stringWithFormat:@"网络请求未知错误：code=%ld", (long)statusCode];
            NSLog(@"%@", statusMessage);
        }
    }
    return statusMessage;
}
@end
