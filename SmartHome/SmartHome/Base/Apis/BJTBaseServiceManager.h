//
//  BJTBaseServiceManager.h
//  SmartHome
//
//  Created by Yale on 2018/11/26.
//  Copyright © 2018年 Kevin. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TBDefaultUATDomain @"http://47.92.126.24:8080"// 开发环境
#define TBReleaseOnlineDomain @"http://47.92.16.244:8080"// 生产环境
@interface BJTBaseServiceManager : NSObject<TBServiceProtocol>

@end
