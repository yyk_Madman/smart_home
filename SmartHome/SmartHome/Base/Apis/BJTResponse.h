//
//  BJTResponse.h
//  SmartHome
//
//  Created by Yale on 2018/11/26.
//  Copyright © 2018年 Kevin. All rights reserved.
//

#import "BJTParseModel.h"

@interface BJTResponse : BJTParseModel
@property (nonatomic, copy) NSString *status; //
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) id data;
@end
