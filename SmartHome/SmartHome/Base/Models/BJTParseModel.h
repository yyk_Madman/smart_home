//
//  BJTParseModel.h
//  SmartHome
//
//  Created by Yale on 2018/11/26.
//  Copyright © 2018年 Kevin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BJTParseModel;

@interface NSObject (Helper)

+ (BJTParseModel *)entityWithData:(id)entityData entityClass:(Class)entityClass;

+ (NSMutableArray *)entityListWithData:(id)entityListData entityClass:(Class)entityClass;

+ (Class)getClassTypeOfProperty:(NSString *)propertyName;

- (NSDictionary *)dictionaryRepresentation;

@end
@interface BJTParseModel : NSObject <NSCopying, NSCoding>

@end
