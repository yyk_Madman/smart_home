//
//  BJTViewController.h
//  SmartHome
//
//  Created by Yale on 2018/11/27.
//  Copyright © 2018年 Kevin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BJTViewController : UIViewController
- (void)addLeftBackTitleButton;
- (void)addLeftBackTitleButton:(NSString *)title;
- (void)addLeftBackImageButton;
- (void)addLeftBackImageButton:(UIImage *)image;
- (void)popViewController;
- (void)addLeftBarButtonWithTitle:(NSString *)title target:(id)target action:(SEL)action;
- (void)addLeftBarButtonWithImage:(UIImage *)image target:(id)target action:(SEL)action;
- (void)addRightBarButtonWithTitle:(NSString *)title target:(id)target action:(SEL)action;
- (void)addRightBarButtonWithImage:(UIImage *)image target:(id)target action:(SEL)action;
@end
